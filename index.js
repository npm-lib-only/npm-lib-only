const path  = require ('path');

const madge = require('madge');

function reducePackageDeps (pkg, dirs, skipFiles) {
	const allDepsFetching = [];

	dirs.forEach (dir => {
		// madge have quite strange output for directories,
		// not including directory names in output
		const dirDepsFetching = madge(dir).then((res) => {


			const secondPass = Object.keys (res.obj()).filter (
				src => skipFiles.indexOf (src) === -1
			).map (src => path.join (dir, src));

			return madge(secondPass);

		}).then ((res) => {

			return res.warnings();

		});

		allDepsFetching.push (dirDepsFetching);
	});

	return Promise.all (allDepsFetching).then (warnings => {

		warnings = {
			skipped: warnings.reduce ((all, w) => {return all.concat (w.skipped)}, [])
		};

		const pkgDeps = pkg.dependencies;
		pkg.dependencies = {};
		Object.keys(pkgDeps).forEach (dep => {
			if (warnings.skipped.indexOf (dep) > -1) {
				pkg.dependencies[dep] = pkgDeps[dep];
			}
		});

		return pkg;
	});

}

module.exports = reducePackageDeps;

if (module === require.main) {
	const pkg = require (path.join (process.cwd(), "package.json"));

	const dirs      = process.env.LIB_DIRS.split (' ') || [];
	const skipFiles = process.env.LIB_SKIP.split (' ') || [];

	reducePackageDeps (pkg, dirs, skipFiles).then (pkg => {
		console.log (JSON.stringify (pkg, null, "\t"));
	});
}
